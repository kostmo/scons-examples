A-priori complete dependency knowledge
========================================================

With this invocation:

    scons --quiet --dry-run --tree=prune my_program

The following tree will be produced:

    +-my_program
      +-hello.o
      | +-hello.c
      | | +-hello.c.template
      | | +-/bin/cp
      | +-hello.c.hdeps
      | +-incdir/myheader.h
      | +-incdir/myheader2.h
      | +-/usr/bin/gcc
      +-impl.o
      | +-impl.c
      | +-incdir/myheader.h
      | +-incdir/myheader2.h
      | +-/usr/bin/gcc
      +-/usr/bin/gcc

