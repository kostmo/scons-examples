A-posteriori dependency knowledge
========================================================

At first, with this invocation:

    scons --quiet --dry-run --tree=prune my_program

The following tree will be produced, which is incomplete:

    +-my_program
      +-hello.o
      | +-hello.c
      | | +-hello.c.template
      | | +-/bin/cp
      | +-/usr/bin/gcc
      +-impl.o
      | +-impl.c
      | +-incdir/myheader.h
      | +-incdir/myheader2.h
      | +-/usr/bin/gcc
      +-/usr/bin/gcc


However, after running a full build:

    scons --quiet

The dry-run dependency tree is now complete. The command:

    scons --quiet --dry-run --tree=prune my_program

Now yields:

    +-my_program
      +-hello.o
      | +-hello.c
      | | +-hello.c.template
      | | +-/bin/cp
      | +-incdir/myheader.h
      | +-incdir/myheader2.h
      | +-/usr/bin/gcc
      +-impl.o
      | +-impl.c
      | +-incdir/myheader.h
      | +-incdir/myheader2.h
      | +-/usr/bin/gcc
      +-/usr/bin/gcc

